import { useDispatch, useSelector } from 'react-redux';
import { decrementAction, incrementAction } from './store/cashReducer';
import './App.css';
import { addCustomerAction, deleteCustomerAction, requestAllCustomersAction } from './store/customerReducer';

function App() {
  const cash = useSelector(state => state.cash.cash);
  const customers = useSelector(state => state.customers.customers);
  const dispatch = useDispatch();

  const handleIncrement = (payload) => {
    dispatch(incrementAction(payload))
  }

  const handleDecrement = (payload) => {
    dispatch(decrementAction(payload))
  }

  const handleAddCustomer = (payload) => {
    const user = {
      id: Date.now(),
      name: payload
    }

    dispatch(addCustomerAction(user))
  }

  const handleDeleteCustomer = (payload) => {
    dispatch(deleteCustomerAction(payload))
  }

  const handleGetAllCustomers = () => {
    dispatch(requestAllCustomersAction())
  }

  return (
    <div className="App">
      <p> My cash: {cash}</p>
      <button onClick={() => handleIncrement(Number(prompt()))}>ADD CASH</button>
      <button onClick={() => handleDecrement(Number(prompt()))}>GET CASH</button>
      <br/>
      {customers.length ?
        (customers.map(customer =>
          <p key={customer.id} onClick={() => handleDeleteCustomer(customer.id)}>{customer.name}</p>)
        ) : (
          <p>We don't have any users</p>
        )
      }
      <button onClick={() => handleAddCustomer(prompt())}>ADD CUSTOMER</button>
      <button onClick={() => handleGetAllCustomers()}>GET CUSTOMERS</button>
    </div>
  );
}

export default App;
