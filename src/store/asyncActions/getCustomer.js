import { getAllCustomersAction } from "../customerReducer"

export const getAllCustomers = () => {
  return dispatch => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => dispatch(getAllCustomersAction(json)))
  }
}
