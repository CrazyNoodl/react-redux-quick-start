export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

const defaultStore = {
  cash: 0,
}

export const incrementAction = (payload) => ({
  type: INCREMENT,
  payload
})

export const decrementAction = (payload) => ({
  type: DECREMENT,
  payload
})

export const cashReducer = (state = defaultStore, action) => {
  const { type, payload } = action;

  switch (type) {
    case INCREMENT:
      return {
        ...state,
        cash: state.cash + payload
      };

    case DECREMENT:
      return {
        ...state,
        cash: state.cash - payload
      };

    default:
      return state;
  }
}

export default cashReducer;
