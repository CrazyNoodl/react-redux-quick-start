export const ADD_CUSTOMER = 'ADD_CUSTOMER';
export const DELETE_CUSTOMER = 'DELETE_CUSTOMER';
export const GET_CUSTOMERS = 'GET_CUSTOMERS';
export const REQUEST_ALL_CUSTOMERS = 'REQUEST_ALL_CUSTOMERS';

const defaultStore = {
  customers: [],
}

export const addCustomerAction = (payload) => ({
  type: ADD_CUSTOMER,
  payload
})

export const deleteCustomerAction = (payload) => ({
  type: DELETE_CUSTOMER,
  payload
})

export const getAllCustomersAction = (payload) => ({
  type: GET_CUSTOMERS,
  payload
})

export const requestAllCustomersAction = () => ({
  type: REQUEST_ALL_CUSTOMERS,
})

const customerReducer = (state = defaultStore, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_CUSTOMERS:
      return {
        ...state,
        customers: [...state.customers, ...payload]
      };

    case ADD_CUSTOMER:
      return {
        ...state,
        customers: [...state.customers, payload]
      };

    case DELETE_CUSTOMER:
      return {
        ...state,
        customers: state.customers.filter(customer => customer.id !== payload)
      };

    default:
      return state;
  }
}

export default customerReducer;
