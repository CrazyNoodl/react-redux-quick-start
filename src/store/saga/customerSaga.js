import { takeEvery, put, call } from 'redux-saga/effects';
import { getAllCustomersAction, REQUEST_ALL_CUSTOMERS } from '../customerReducer';

function* customerWorker () {
  const data = yield call(() => fetch('https://jsonplaceholder.typicode.com/users'));
  const customers = yield call(() => new Promise(res => res(data.json())))
  yield put(getAllCustomersAction(customers))
}

export function* customerWatcher() {
  yield takeEvery(REQUEST_ALL_CUSTOMERS, customerWorker);
}
